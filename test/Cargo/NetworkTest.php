<?php
namespace Cargo;

use \Cargo\Storage\ArrayLike;

class NetworkTest extends  \PHPUnit_Framework_TestCase {

    /**
     * @var Network
     */
    protected $network;

    protected function setup() {
        $storage = ArrayLike::initWithJSON( __DIR__ . '/../data/data.json');
        $this->network = new Network($storage);
    }

    public function test1stRound() {
        $friends = $this->network->getPersonFriends(5);

        $this->assertContainsOnlyInstancesOf('Cargo\Person', $friends);
        $this->assertCount( 5 , $friends);

        $friends_ids = array_map( function (Person $p) {return $p->getId();} , $friends  );

        $this->assertContains( 3 , $friends_ids);
        $this->assertContains( 6 , $friends_ids);
        $this->assertContains( 11 , $friends_ids);
        $this->assertContains( 10 , $friends_ids);
        $this->assertContains( 7 , $friends_ids);
    }

    public function test2ndRound() {
        $friends = $this->network->getPerson2ndRound(7);

        $this->assertContainsOnlyInstancesOf('Cargo\Person', $friends);
        $this->assertCount( 10 , $friends);

        $friends_ids = array_map( function (Person $p) {return $p->getId();} , $friends  );

        $this->assertContains( 2 , $friends_ids);
        $this->assertContains( 4 , $friends_ids);
        $this->assertContains( 6 , $friends_ids);
        $this->assertContains( 10 , $friends_ids);
        $this->assertContains( 11 , $friends_ids);
        $this->assertContains( 13 , $friends_ids);
        $this->assertContains( 16 , $friends_ids);
        $this->assertContains( 17 , $friends_ids);
        $this->assertContains( 19 , $friends_ids);
        $this->assertContains( 9 , $friends_ids);
    }

    public function testSuggested() {
        $friends = $this->network->getSuggestedFriends(20);

        $this->assertContainsOnlyInstancesOf('Cargo\Person', $friends);
        $this->assertCount( 2 , $friends);

        $friends_ids = array_map( function (Person $p) {return $p->getId();} , $friends  );

        $this->assertContains( 18 , $friends_ids);
        $this->assertContains( 5 , $friends_ids);
    }

    public function testSuggestedEmpty() {
        $friends = $this->network->getSuggestedFriends(1);
        $this->assertEmpty($friends);
    }

}