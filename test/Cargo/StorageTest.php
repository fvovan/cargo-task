<?php
namespace Cargo;

use \Cargo\Storage\ArrayLike;

class StorageTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Storage\IStorage
     */
    protected $storage;

    protected function setUp() {
        $this->storage = new ArrayLike( [ 'a' ,  'b' ] );
    }

    /**
     * @expectedException \OutOfBoundsException
     * @small
     */
    public function  testStorageFail() {
        $this->storage->getPersonById(4);
    }
    /**
     * @small
     */
    public function  testStorageFound() {
        $this->assertEquals( 'a', $this->storage->getPersonById(0));
        $this->assertEquals( 'b', $this->storage->getPersonById(1));
    }
}