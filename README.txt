Task:
    Direct friends: Return those people who are directly connected to the chosen person.
    Friends of friends: Return those who are two steps away, but not directly connected to the chosen person.
    Suggested friends: Return people in the group who know 2 or more direct friends of the chosen person, but are not directly connected to her.

Requires:
    PHP >= 5.5.0
    composer

Installing:
    composer install

Running:
    php run.php {person_id} {first|second|suggested}
    example:
        php run.php 2 first
        php run.php 5 suggested

Testing:
    $ cd test
    $ ../vendor/bin/phpunit
    (check you have execution permission for it)