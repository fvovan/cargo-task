<?php
    if (version_compare(phpversion(), '5.5.0', '<')) {
        trigger_error("PHP version >= 5.5 required ", E_USER_ERROR);
    }

    if ($argc < 3 || empty($argv[1]) || empty($argv[2])) {
        trigger_error("Please Provide person ID and one of 3 Methods ('first', 'second', 'suggested') ", E_USER_ERROR);
    }
    require 'vendor/autoload.php';

    $storage =  \Cargo\Storage\ArrayLike::initWithJSON(  __DIR__ . '/data/data.json');
    $network =  new \Cargo\Network($storage);

    $person_id = (int)$argv[1];

    switch (trim($argv[2])) {
        case 'first':
            echo json_encode($network->getPersonFriends($person_id), JSON_PRETTY_PRINT);
            break;
        case 'second':
            echo json_encode($network->getPerson2ndRound($person_id), JSON_PRETTY_PRINT);
            break;
        case 'suggested':
            echo json_encode($network->getSuggestedFriends($person_id), JSON_PRETTY_PRINT);
            break;
        default:
            trigger_error("Invalid Method ", E_USER_ERROR);
            break;
    }