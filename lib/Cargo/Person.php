<?php
namespace Cargo;

/**
 * Class Person
 * @package Cargo
 */
class Person implements  \JsonSerializable {

    private $id, $first_name, $last_name, $age, $gender, $friends_ids;

    /**
     * @param $id
     * @param $first_name
     * @param $last_name
     * @param $age
     * @param $gender
     * @param Array $friends_ids
     */
    public function __construct($id, $first_name, $last_name, $age, $gender, Array $friends_ids = array()) {
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->age = $age;
        $this->gender = $gender;
        $this->friends_ids = $friends_ids;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getAge() {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * @return array
     */
    public function getFriendsIds() {
        return $this->friends_ids;
    }


    /**
     * @param array $json
     * @return Person
     */
    public static function fromArray(array $json) {
        return new self($json['id'], $json['firstName'], $json['surname'], $json['age'], $json['gender'], $json['friends']);
    }

    /**
     * @return array
     */
    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'firstName' => $this->getFirstName(),
            'surname'  =>  $this->getLastName(),
            'age'       => $this->getAge(),
            'gender'    => $this->getGender(),
            'friends'   => $this->getFriendsIds(),
        ];
    }

}