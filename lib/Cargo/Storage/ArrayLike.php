<?php
namespace Cargo\Storage;

use Cargo\Person;

/**
 * Class ArrayLike
 * @package Cargo\Storage
 */
class ArrayLike implements IStorage {
    /**
     * @var array
     */
    protected $persons;

    /**
     * @param array $persons
     */
    public function __construct(Array $persons) {
        $this->persons = $persons;
    }

    /**
     * @param $person_id
     * @return Person
     * @throws \OutOfBoundsException
     */
    public function getPersonById($person_id) {
        if (array_key_exists( $person_id, $this->persons )) {
            return $this->persons[$person_id];
        }
        else {
            throw new \OutOfBoundsException("Person $person_id is not found in Storage");
        }
    }

    /**
     * @param string $file
     * @return ArrayLike
     *
     * "Fabric" for Json file
     */
    public static function initWithJSON($file) {
        $json_string = file_get_contents($file);

        if ($json_string === false) {
            throw new \RuntimeException('Can\'t read file '.$file);
        }

        $json_persons = json_decode($json_string, true);

        if (json_last_error()) {
            throw new \RuntimeException('JSON parsing error '.json_last_error());
        }

        $persons = array();
        foreach($json_persons as $person) {
            $persons[$person['id']] = Person::fromArray( $person );
        }

        return new self($persons);
    }
}