<?php
namespace Cargo\Storage;

/**
 * Interface IStorage
 * @package Cargo\Storage
 */
interface IStorage {

    /**
     * @param $id
     * @return \Cargo\Person
     * @throws \OutOfBoundsException
     */
    function getPersonById($id);
}