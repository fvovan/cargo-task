<?php
namespace Cargo;

use Cargo\Storage\IStorage;

/**
 * Class Network
 * @package Cargo
 */
class Network {

    /**
     * @var IStorage
     */
    protected $storage;

    /**
     * @param IStorage $storage
     */
    public function  __construct(IStorage $storage) {
        $this->storage = $storage;
    }

    /**
     * @param array $ids
     * @return array
     *
     * Find several persons in storage
     */
    protected function mapPersonsByIds(array $ids) {
        return array_map( function($id) { return $this->storage->getPersonById($id);} , $ids);
    }

    /**
     * @param $person_id
     * @return array
     *
     * Find person in storage
     */
    protected function getPersonFriendsIds($person_id) {
        return $this->storage->getPersonById($person_id)->getFriendsIds();
    }

    /**
     * @param $person_id
     * @return array
     */
    protected function getPerson2ndRoundIds($person_id) {
        $first_round_ids = $this->getPersonFriendsIds($person_id);
        $second_round_ids = [];

        foreach ($first_round_ids as $first_friend_id) {
            $second_round_ids = array_merge($second_round_ids, $this->getPersonFriendsIds($first_friend_id));
        }

        return array_values(
                 array_unique(
                    array_diff( $second_round_ids, array_merge($first_round_ids , [$person_id] ) )
                    //remove 1st round and myself
                 ) //eliminate duplicates
        ); //fix array indexes
    }

    /**
     * @param $person_id
     * @return array
     *
     * Find direct friends
     */
    public function getPersonFriends($person_id) {
        return $this->mapPersonsByIds( $this->getPersonFriendsIds($person_id) );
    }

    /**
     * @param $person_id
     * @return array
     *
     * Find "second round" friends
     */
    public function getPerson2ndRound($person_id) {
        return $this->mapPersonsByIds( $this->getPerson2ndRoundIds($person_id) );
    }


    /**
     * @param $person_id
     * @param int $common_friends
     * @return array
     *
     * Find suggested friends
     */
    public function getSuggestedFriends($person_id, $common_friends = 2) {

        $first_round_ids = $this->getPersonFriendsIds($person_id);

        if (sizeof($first_round_ids) < $common_friends) {
            return []; //man without enough own friends can't have suggested
        }

        $suggested = [];
        foreach ($this->getPerson2ndRoundIds($person_id) as $round2_id) {

            $friend_counter = 0;

            foreach($this->getPersonFriendsIds($round2_id) as $third_id ) {
                if ( in_array($third_id,$first_round_ids ) ) {
                    $friend_counter += 1;
                }

                if ($friend_counter >= $common_friends ) {
                    $suggested[] = $round2_id;
                    break;
                }
            }
        }

        return $this->mapPersonsByIds( $suggested );
    }

}